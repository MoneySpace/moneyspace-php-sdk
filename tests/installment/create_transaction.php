<?php

$result = "";

if (!empty($_POST)) {
    // it have request data
    $secret_id = $_POST["secret_id"];
    $secret_key = $_POST["secret_key"];
    $order_id = $_POST["order_id"];
    $firstname = $_POST["firstname"];
    $lastname = $_POST["lastname"];
    $email = $_POST["email"];
    $phone = $_POST["phone"];
    $amount = doubleval($_POST["amount"]);
    $description = $_POST["description"];
    $address = $_POST["address"];
    $message = $_POST["message"];
    $feeType = $_POST["feeType"];
    
    $success_Url = $_POST["success_Url"];
    $fail_Url = $_POST["fail_Url"];
    $cancel_Url = $_POST["cancel_Url"];
    $agreement = $_POST["agreement"];
    $bankType = $_POST["bankType"];
    $startTerm = $_POST["startTerm"];
    $endTerm = $_POST["endTerm"];

    $data = array("secret_id" => $secret_id
        , "secret_key" => $secret_key
        , "firstname" => $firstname
        , "lastname" => $lastname
        , "email" => $email
        , "phone" => $phone
        , "amount" => $amount
        , "description" => $description
        , "address" => $address
        , "message" => $message
        , "feeType" => $feeType
        , "order_id" => $order_id
        , "success_Url" => $success_Url
        , "fail_Url" => $fail_Url
        , "cancel_Url" => $cancel_Url
        , "agreement" => $agreement
        , "bankType" => $bankType
        , "startTerm" => $startTerm
        , "endTerm" => $endTerm);
    
    $result = create_installment_transaction($data);
}


function create_installment_transaction($data) {
    $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://a.moneyspace.net/payment/Createinstallment',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS =>$data
    ));
    $response = curl_exec($curl);
    curl_close($curl);

    return $response;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Installment Document</title>
</head>
<body>
    <div class="container">
    <div class="container text-center">
        <h1>Installment example</h1>
    </div>
    <a href="/index.php">Go Home</a> | 
    <a href="/samples_code/create_transaction_installment.md" target="blank">View the complete code</a>
    <form method="post">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="secret_id">secret_id *</label>
                <input type="text" class="form-control" id="secret_id" name="secret_id" required>
            </div>
            <div class="form-group col-md-6">
                <label for="secret_key">secret_key *</label>
                <input type="text" class="form-control" id="secret_key" name="secret_key" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="order_id">order_id *</label>
                <input type="text" class="form-control" id="order_id" name="order_id" value="<?php echo "EX".date("YmdHis"); ?>" required>
            </div>
            <div class="form-group col-md-6">
                <label for="feeType">feeType</label>
                <select name="feeType" id="feeType" class="form-control" >
                <option value="include">include</option>
                <option value="exclude">exclude</option>
                </select>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="firstname">firstname</label>
                <input type="text" class="form-control" id="firstname" name="firstname" value="John" required>
            </div>
            <div class="form-group col-md-6">
                <label for="lastname">lastname</label>
                <input type="text" class="form-control" id="lastname" name="lastname" value="Exmark" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="email">email</label>
                <input type="email" class="form-control" id="email" name="email" value="test@gmail.com" required>
            </div>
            <div class="form-group col-md-6">
                <label for="phone">phone</label>
                <input type="tel" class="form-control" id="phone" name="phone" value="0923456789" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="amount">amount</label>
                <input type="number" class="form-control" id="amount" name="amount" value="3001" required>
            </div>
            <div class="form-group col-md-6">
                <label for="description">description</label>
                <input type="text" class="form-control" id="description" name="description" value="Belt 3001u0e3f ( 1 qty )" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="address">address</label>
                <textarea name="address" class="form-control" id="address" cols="30" rows="5" required>LH 164/55 House No. 39/112 Village No., Phetchaburi Rd., Makkasan, Ratchathewi, Bangkok, 10400</textarea>
            </div>
            <div class="form-group col-md-6">
                <label for="message">message</label>
                <textarea name="message" class="form-control" id="message" cols="30" rows="5"></textarea>
            </div>
        </div>
        <!-- <div class="form-group">
            <label for="feeType">feeType</label>
            <select name="feeType" id="feeType" class="form-control" >
            <option value="include">include</option>
            <option value="exclude">exclude</option>
            </select>
        </div> -->
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="agreement">agreement</label>
                <input type="text" class="form-control" id="agreement" name="agreement" value="1" readonly>
            </div>
            <div class="form-group col-md-6">
                <label for="bankType">bankType</label>
                <select class="form-control" id="bankType" name="bankType">
                <option value="KTC">KTC</option>
                <option value="BAY" selected>BAY</option>
                <option value="FCY">FCY</option>
                </select>
                <!-- <input type="text" class="form-control" id="bankType" name="bankType" value="BAY"> -->
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="startTerm">startTerm</label>
                <input type="text" class="form-control" id="startTerm" name="startTerm" value="3" required>
            </div>
            <div class="form-group col-md-6">
                <label for="endTerm">endTerm</label>
                <input type="text" class="form-control" id="endTerm" name="endTerm" value="10" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="success_Url">success_Url</label>
                <input type="text" class="form-control" id="success_Url" name="success_Url" value="https://www.yourwebsite.com/success" required>
            </div>
            <div class="form-group col-md-4">
                <label for="fail_Url">fail_Url</label>
                <input type="text" class="form-control" id="fail_Url" name="fail_Url" value="https://www.yourwebsite.com/fail" required>
            </div>
            <div class="form-group col-md-4">
                <label for="cancel_Url">cancel_Url</label>
                <input type="text" class="form-control" id="cancel_Url" name="cancel_Url" value="https://www.yourwebsite.com/cancel" required>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">submit</button>
        </form>
        <hr>
        <h2>Result</h2>
        <pre class="alert alert-secondary" style="height: 400px; white-space: pre-wrap; white-space: -moz-pre-wrap; white-space: -pre-wrap; white-space: -o-pre-wrap; word-wrap: break-word;">
        <?php 
            print_r($result);
        ?>
        </pre>
    </div>
</body>
<footer>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</footer>
</html>