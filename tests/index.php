<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Moneyspace Document</title>
</head>
<body>
    <h1>Payment Example</h1>
    <ul>
        <li>Open Installment example: <a href="./installment/create_transaction.php">click here</a></li>
        <li>Open QR Code example: <a href="./qrcode/create_transaction.php">click here</a></li>
        <li>Open Credit Card example: <a href="./creditcard/create_transaction.php">click here</a></li>
        <li>Open Check Payment example: <a href="./check_payment.php">click here</a></li>
        <li>Open Check Order ID example: <a href="./check_orderId.php">click here</a></li>
        <li>Open Cancel Payment example: <a href="./cancel_payment.php">click here</a></li>
    </ul>
    
</body>
</html>